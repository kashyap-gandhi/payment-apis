<?php

require_once 'anet_php_sdk/AuthorizeNet.php'; // The SDK

// Edit this to be the url of this script

$url = "http://www.yoursite.com/easy_pay/easy_pay.php";

$api_login_id = 'your api login id';
$transaction_key = 'your transaction key';
$md5_setting = 'your MD5 setting'; 


$buy_form = "ep_form.php";
$receipt_form = "ep_receipt.php";

// ----------------------------------------------------------------
// DO NOT EDIT BELOW THIS POINT!
// ----------------------------------------------------------------

$test_mode = false;  //Don't change this unless you have a test account

$LIVE_URL = 'https://secure.authorize.net/gateway/transact.dll';
$SANDBOX_URL = 'https://test.authorize.net/gateway/transact.dll';

// Show CC form
if($_REQUEST['action'] == 'payment') {
	    
            $fp_sequence = time(); // Any sequential number like an invoice number.
            $time = time();
	    $amount = $_REQUEST['amount'];
	    if(!is_numeric($amount)) {
		redirect_me($url);
		exit;
	    }
	    $note = $_REQUEST['note'];
            $sim = new AuthorizeNetSIM_Form(
            array('x_amount'=> $amount,'x_fp_sequence'=> $fp_sequence,'x_fp_timestamp'=> $time,'x_relay_response'=> "TRUE",'x_relay_url'=> $url,'x_login'=> $api_login_id));
            $fp = $sim->getFingerprint($api_login_id, $transaction_key, $amount, $fp_sequence, $time);
	    $sim->x_fp_hash = $fp;
           $hidden_fields = $sim->getHiddenFieldString();
           $post_url = ($test_mode ? $SANDBOX_URL : $LIVE_URL);
	    // Show form here
	   $content_file = $buy_form;
}
// Get answer back from Authorize.net
elseif ($_POST['x_response_code'] != "") {
	    // Check incoming variables
            $response = new AuthorizeNetSIM($api_login_id, $md5_setting);
            if ($response->isAuthorizeNet()) 
            {
                if ($response->approved) 
                {
                    // Do your processing here.
                    $redirect_url = $url . '?response_code=1&transaction_id=' . $response->transaction_id ."&amount=".$response->amount; 
                }
                else
                {
                    // Redirect to error page.
                    $redirect_url = $url . '?response_code='.$response->response_code . '&response_reason_text=' . $response->response_reason_text;
                }
                // Send the Javascript back to AuthorizeNet, which will redirect user back to your site.
		redirect_me($redirect_url);
            }
            else
            {
                echo "Error -- not AuthorizeNet. Check your MD5 Setting.";
            }
	    exit;
}
// show receipt page 
elseif ($_GET['transaction_id'] != '' || $_GET['response_reason_text'] != "") {
            if ($_GET['response_code'] == 1)
            {
                $transaction =  htmlentities($_GET['transaction_id']);
                $amount =  htmlentities($_GET['amount']);
		$content_file = $receipt_form;
            }
            else
            {
              ?><script>
	      alert("An error occurred: <?=htmlentities($_GET['response_reason_text']);?>");
	      history.go(-2);
	      </script>
	      <?
	      exit;
            }
        }
else {
 $content_file = $buy_form;

}

include $content_file;

function redirect_me ($u) {
		?>
        <html><head><script language="javascript">
                <!--
                window.location="<?=$u;?>";
                //-->
                </script>
                </head><body><noscript><meta http-equiv="refresh" content="1;url=<?=$u;?>"></noscript></body></html>
		<?
}
?>
