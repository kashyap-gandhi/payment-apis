<?php
         
    //////////=============authorize .net aim================
        
    function auth_net_aim($pid=null,$amount=null)
    {
        $site_setting = site_setting();
        $wallet_setting = wallet_setting();
        $minimum_amount=$wallet_setting->wallet_minimum_amount;
        $chk_amt=$amount;
        
        if($chk_amt<$minimum_amount)
        {
            redirect('wallet/add_wallet/fail');        
        }
        
    
        
        $this->load->library('form_validation');        
        $this->form_validation->set_rules('x_card_num', CREDIT_CARD_NUMBER, 'required');
        $this->form_validation->set_rules('x_exp_date', EXPIRE_DATE, 'required');
        $this->form_validation->set_rules('x_card_code', CCV, 'required|numeric|exact_length[3]');
        $this->form_validation->set_rules('x_first_name', FIRST_NAME, 'required|alpha_space');
        $this->form_validation->set_rules('x_last_name', LAST_NAME, 'required|alpha_space');
        $this->form_validation->set_rules('x_address',ADDRESS, 'required');
        $this->form_validation->set_rules('x_city', CITY, 'required|alpha_space');
        $this->form_validation->set_rules('x_state', STATE, 'required|alpha_space');
        $this->form_validation->set_rules('x_country', COUNTRY, 'required|alpha_space');
        $this->form_validation->set_rules('x_zip', ZIPCODE, 'required|numeric');

    if($this->form_validation->run() == FALSE)
    {    
    
        if(validation_errors())
        {
            $data['error'] = validation_errors();
        }
        else
        {
            $data["error"] = '';
        }
                
                
                $data['total_wallet_amount']=$this->home_model->my_wallet_amount(); 
                $data['view'] = "login";
                $data['view'] = "signup";
            
                $data['header_menu'] = dynamic_menu(0);
                $data['footer_menu'] = dynamic_menu_footer(0);
                $data['right_menu'] = dynamic_menu_right(0);
                $data['site_setting'] = site_setting();    
                $meta = meta_setting();
                $user = get_user_detail($this->session->userdata('user_id'));
                
            
            $data['wallet_setting'] = wallet_setting();
            $data['total_wallet_amount']=$this->home_model->my_wallet_amount(); 


                
                $data['pid']=$pid;
                $data['amount']=$amount;
                
                
                $this->home_model->select_text($this->session->userdata('lang_id'));
                
                $this->template->write('meta_title','Authorize.net Payment-'.$user['user_name'].' '.$user['last_name'].' - '.$meta['title'], TRUE);
                $this->template->write('meta_description','Authorize.net Payment-'.$user['user_name'].' '.$user['last_name'].' - '. $meta['meta_description'], TRUE);
                $this->template->write('meta_keyword', 'Authorize.net Payment-'.$user['user_name'].' '.$user['last_name'].' - '.$meta['meta_keyword'], TRUE);
                $this->template->write_view('header', 'default/header_login', $data, TRUE);
                $this->template->write_view('center_content', 'default/wallet/direct_post', $data, TRUE);
                $this->template->write_view('footer', 'default/footer',$data, TRUE);
                $this->template->render();
                
                                
                
                
            }
            else{
                $i=0;
            
            $x_card_num=$this->input->post('x_card_num');
            $x_exp_date=$this->input->post('x_exp_date');
            $x_card_code=$this->input->post('x_card_code');
            
            $x_first_name=$this->input->post('x_first_name');
            $x_last_name=$this->input->post('x_last_name');
            $x_address=$this->input->post('x_address');
            
            $x_city=$this->input->post('x_city');
            $x_state=$this->input->post('x_state');
            $x_zip=$this->input->post('x_zip');
            $x_country=$this->input->post('x_country');
            
            $x_login=$this->get_gateway_result($pid,'x_login');
            $x_tran_key=$this->get_gateway_result($pid,'x_tran_key');
            $x_type=$this->get_gateway_result($pid,'x_type');
            $x_method=$this->get_gateway_result($pid,'x_method');
            $x_description=$this->get_gateway_result($pid,'x_description');
            
            $modname='wallet';
             $site_status=$this->get_gateway_result($pid,'site_status');    
            
            $post_url = "https://test.authorize.net/gateway/transact.dll";
            
            if($site_status['value']=='sandbox')
            {
                $post_url = "https://test.authorize.net/gateway/transact.dll";
                $test_request=TRUE;
            }
            else
            {
                $post_url = "https://secure.authorize.net/gateway/transact.dll";
                $test_request=TRUE;
            }
        $wallet_setting = wallet_setting();
        $wallet_add_fees=$wallet_setting->wallet_add_fees;    
        $add_fees= number_format((($amount * $wallet_add_fees)/100),2);
            
        $total = number_format(($add_fees + $amount),2);
        
        
        
        $post_values = array(
            
                                
            // the API Login ID and Transaction Key must be replaced with valid values
            "x_login"            => $x_login['value'],
            "x_tran_key"        => $x_tran_key['value'],
        
            "x_version"            => "3.1",
            "x_delim_data"        => "TRUE",
            "x_delim_char"        => "|",
            "x_relay_response"    => "FALSE",
        
            "x_type"            => $x_type['value'],
            "x_method"            => $x_method['value'],
            "x_card_num"        => $x_card_num,
            "x_exp_date"        => $x_exp_date,
        
            "x_amount"            => $total,
            "x_description"        => $x_description['value'],
        
            "x_first_name"        => $x_first_name,
            "x_last_name"        => $x_last_name,
            "x_address"            => $x_address,
            "x_state"            => $x_state,
            "x_zip"                => $x_zip,
            "x_modname"            => $modname,
            "x_test_request"    => $test_request
            // Additional fields can be added here as outlined in the AIM integration
            // guide at: http://developer.authorize.net
        );
        //echo $x_login['value'].'='.$x_tran_key['value'].'='.$x_type['value'].'='.$x_method['value'].'='.$x_description['value'];
        //exit;
        // This section takes the input fields and converts them to the proper format
        // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
        $post_string = "";
        foreach( $post_values as $key => $value )
            { $post_string .= "$key=" . urlencode( $value ) . "&"; }
        $post_string = rtrim( $post_string, "& " );
        
        
        
        
        
        // The following section provides an example of how to add line item details to
        // the post string.  Because line items may consist of multiple values with the
        // same key/name, they cannot be simply added into the above array.
        //
        // This section is commented out by default.
        /*
        $line_items = array(
            "item1<|>golf balls<|><|>2<|>18.95<|>Y",
            "item2<|>golf bag<|>Wilson golf carry bag, red<|>1<|>39.99<|>Y",
            "item3<|>book<|>Golf for Dummies<|>1<|>21.99<|>Y");
            
        foreach( $line_items as $value )
            { $post_string .= "&x_line_item=" . urlencode( $value ); }
        */
        
        // This sample code uses the CURL library for php to establish a connection,
        // submit the post, and record the response.
        // If you receive an error, you may want to ensure that you have the curl
        // library enabled in your php configuration
        $request = curl_init($post_url); // initiate curl object
            curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
            curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
            curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
            curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
            $post_response = curl_exec($request); // execute curl post and store results in $post_response
            // additional options may be required depending upon your server configuration
            // you can find documentation on curl options at http://www.php.net/curl_setopt
        curl_close ($request); // close curl object
        
        // This line takes the response and breaks it into an array using the specified delimiting character
        $response_array = explode($post_values["x_delim_char"],$post_response);
        
        
        // The results are output to the screen in the form of an html numbered list.
    //echo $response_array[0];
        $strtemp='';
        foreach ($response_array as $key => $value)
        {
            $strtemp .= $key."===".$value . "<br/>";        
        }            
        
        
        log_message('error',"Authorize.net(AIM) WALLET RESPONSE DATA:".$strtemp);
        
        $res_num=$response_array[0];
        
        
        if($res_num=='1')
        {
        
            $transaction_id=$response_array[6];        
            
            if($transaction_id==0 || $transaction_id=='0')
            {
                $chk_transaction_id=0;
            }
            else
            {                    
                $chk_transaction_id=$this->check_unique_transaction($transaction_id);
            }
        
            if($chk_transaction_id==0){
            
        
                $debit=$response_array[9];
                $transaction_id=$response_array[6];                        
        
                    
                    $pay_gross=$debit;
                    $gateway_id=$pid;
                    $payer_status='Paid';
                    $txnid=$transaction_id;
                    
                    $user_id=$this->session->userdata('user_id');
                    $date=date('Y-m-d H:i:s');
                    $ip=$_SERVER['REMOTE_ADDR'];
        
        
        
        $admin_status='Review';
                
                
        $chk_status_admin=$this->db->query("select * from wallet where user_id='".$user_id."' and admin_status='Confirm'");
        
        if($chk_status_admin->num_rows()>3)
        {
                $admin_status='Confirm';
        }
            
                    $data=array(
                        'debit' => $pay_gross,
                        'user_id' => $user_id,
                        'status' => $payer_status,
                        'admin_status' => $admin_status,
                        'wallet_date' => $date,
                        'wallet_transaction_id' => $txnid,
                        'wallet_payee_email' => 'Athorize.Net',
                        'wallet_ip' => $ip    ,
                        'gateway_id' => $gateway_id,
                        'donate_status' => '1'                
                        );
                        
                        
                    $add_wallet=$this->db->insert('wallet',$data);    
        
        
        
            $email_template=$this->db->query("select * from `email_template` where task='New Fund Admin Notification'");
            $email_temp=$email_template->row();                
            
            $email_address_from=$email_temp->from_address;
            $email_address_reply=$email_temp->reply_address;
            $email_subject = WALLET_PAYMENT_PROCESS_FAILED;
            $email_to = $login_user['email'];
            
            $user_id=$this->session->userdata('user_id');
            $login_user = get_user_detail($this->session->userdata('user_id'));
        
        
            $str = sprintf(WALLET_HELLO_USER_YOUR_WALLET_PAYMENT_PROCESS_SUCCESS_WAIT_UNTIL_ADMINISTRATOR_REVIEW,$login_user['user_name']);
            email_send($email_address_from,$email_address_reply,$email_to,$email_subject,$str);       
            
            $msg='<p style="color:#7DBF0F; font-weight:bold;">'.WALLET_AMOUNT_ADDED_IN_WALLET_SUCCESSFULLY_WAIT_FOR_ADMINISTRATOR_REVIEW.'</p>';
            redirect('wallet/my_wallet/0/'.$msg);
            
            
            }
            
            else
            {
                $msg=OUR_PAYMENT_PROCESS_DECLINED_DUE_TO_TRANSACTIONID_ALREADY_EXISTS;
                redirect('wallet/my_wallet/0/'.$msg);
            }
            
        }
        
        elseif($res_num=='2'){
                                
            $msg=$response_array[3];
            redirect('wallet/my_wallet/0/'.$msg);
        }
        
        elseif($res_num=='3'){
                                
            $msg=$response_array[3];
            redirect('wallet/my_wallet/0/'.$msg);
        }
        elseif($res_num=='4'){
                                
            $msg=PAYMENT_PROCESS_HELD_FOR_REVIEW;
            redirect('wallet/my_wallet/0/'.$msg);
        }
        else
		{
			$msg=YOUR_PAYMENT_PROCESS_DECLINED;
			redirect('wallet/my_wallet/0/'.$msg);
		}
        
    }
        
    }

    //////////=============authorize .net aim================
    
    ?>
