<?php

///=======http://www.johnconde.net/blog/tutorial-integrate-the-authorize-net-arb-api-with-php/
require_once("AuthnetARB.class.php");

$login = 'cnpdev4289'; 
$transkey = 'SR2P8g4jdEn7vFLQ'; 
$test = TRUE;

$arb = new AuthnetARB($login, $transkey, $test);

$arb->setParameter('interval_length', 1); 
$arb->setParameter('interval_unit', 'months'); 

$arb->setParameter('startDate', date("Y-m-d")); 

$arb->setParameter('totalOccurrences', 12); ///////=== total occurance 11 month

$arb->setParameter('trialOccurrences', 0); ///////set 1 month only
$arb->setParameter('trialAmount', 0.00); /////== set 1 month amount


$arb->setParameter('amount', 1.00); /////=== single transaction amount
$arb->setParameter('refId', 15);  /////===== normally our unique order no.

$arb->setParameter('cardNumber', '5424000000000015'); 
$arb->setParameter('expirationDate', '2009-05');


$arb->setParameter('firstName', 'Joe'); 
$arb->setParameter('lastName', 'Doe');
$arb->setParameter('address', 'Casa 1872'); 

$arb->setParameter('city', 'City'); 
$arb->setParameter('state', 'FL'); 
$arb->setParameter('zip', '33619'); 
$arb->setParameter('country', 'us');

$arb->setParameter('subscrName', 'The Test Account'); /////custom name (always pass client email and name)

$arb->createAccount();

echo 'isSuccessful: ' .$arb->isSuccessful() . '<br />';

if($arb->isSuccessful()) { 
	echo 'cool, it worked! <br />'; 
} else { 
	echo 'error in payment <br />'; 
}

echo 'isError: ' .$arb->isError() . '<br />'; 
echo 'getSubscriberID: ' .$arb->getSubscriberID() . '<br />'; 
echo 'getResponse: ' .$arb->getResponse() . '<br />'; 
echo 'getResultCode:' .$arb->getResultCode() . '<br />'; 
echo 'getString: ' .$arb->getString() . '<br />'; 
echo 'getRawResponse: ' .$arb->getRawResponse() . '<br />'; 

?>