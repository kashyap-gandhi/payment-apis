<?php

/*Content Page Block */

?>			 

<?php include("header.php");?>
   <script type="text/javascript">
    	
	jQuery.noConflict();
	(function($) {
	$(document).ready(function() { 
		clearForm();
		$('.msgAlert').hide();      
	});
	})(jQuery);

    </script>
<script type="text/javascript">

function numvalidate() {
    v_num = $('#num').val();            
    v_regex = /^[0-9\\-]{10,15}$/;     
    /* only number: /^[0-9]+$/; */  
    /* only character: /^[a-zA-Z]+$/; */  
    if(!v_regex.test(v_num)){  
        return false;  
    }else{  
       return true;
    }  
 }

var unck = jQuery.noConflict();
function clearForm(form)
{
    unck(":input", form).each(function()
    {
    var type = this.type;
    var tag = this.tagName.toLowerCase();
        if (type == 'text')
        {
        this.value = "";

	unck('#message').value = '';

		unck('input[type=checkbox]').attr('checked', false);
        }
    });
};

</script>

<script type="text/javascript">
  var vld = jQuery.noConflict();
function showMessage() { 
      var name = vld("input#name").val();  
	  var cname = vld("input#cname").val();
         var num = vld("#num").val();
	   var message = vld("#message").val();  
	    var email = vld("input#email").val();  
		 var puzzle = vld("input#puzzle").val(); 
        if (name == "" || cname == "" || message == "" || num == "" || numvalidate() == false || email == "" || puzzle != 8) {  
		vld(".msgAlert").show();  
		vld(".msgAlert").fadeOut(5000);
	     vld('html, body').animate({scrollTop:0}, 'slow');
        return false;
    }      
}
</script>
		
    <script type="text/javascript">
            /* <![CDATA[ */
			var inl = jQuery.noConflict();
            inl(function(){
                inl("#num").validate({
                    expression: "if (VAL.match(/^[0-9\\-]{10,15}$/)) return true; else return false;",
                    message: "<img class='close1' src='images/24-em-cross.png' />"
                });
                inl("#email").validate({
                    expression: "if (VAL.match(/^[^\\W][a-zA-Z0-9\\_\\-\\.]+([a-zA-Z0-9\\_\\-\\.]+)*\\@[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)*\\.[a-zA-Z]{2,4}$/)) return true; else return false;",
                    message: "<img class='close1' src='images/24-em-cross.png' />"
                });
				inl("#cname").validate({
                    expression: "if (VAL.match(/^[a-zA-Z]/)) return true; else return false;",
                    message: "<img class='close1' src='images/24-em-cross.png' />"
                });
				inl("#name").validate({
                    expression: "if (VAL.match(/^[a-zA-Z]/)) return true; else return false;",
                    message: "<img class='close1' src='images/24-em-cross.png' />"
                });
				inl("#message").validate({
                    expression: "if (VAL.match(/^[^\\W\\_\\-\\.]/)) return true; else return false;",
                    message: "<img class='close1' src='images/24-em-cross.png' />"
                });
				inl("#puzzle").validate({
                    expression: "if (VAL.match(/[8]/)) return true; else return false;",
                    message: "<img class='close1' src='images/24-em-cross.png' />"
                });
                inl("#Mobile").validate({
                    expression: "if (VAL.match(/^[0-9]{10}$/)) return true; else return false;",
                    message: "<img class='close1' src='images/24-em-cross.png' />"
                });
            });
            /* ]]> */
        </script>		
<link rel="stylesheet" href="form_style.css" type="text/css" />

<div class="homepage-social">
			<div class="homepage-social-gradient gradient-common-blue">
				<div class="homepage-social-feature">
					
				<div class="top-head">
					<h1>Contact US</h1>
				</div>
				
				<div class="first-block-left" >
				
				 <?php include("sidebar.php");?>
				
				</div>
				
				<div class="first-block-center">
				
            
	<div class="msgAlert">An error occurred while processing request</div>		
	
			<div id="formleft">
	<form id="submitform" name="submitform" action="contemails.php" method="post">
	<input id="private" type="hidden" name="private" value="General">
	<table width="315" cellspacing="3" cellpadding="0" class="table">
		<tr>		
		<td>	
		<div class="field">
		  <p>
		   Company Name :
		  </p>
		</div>					
		<input class="inputbox" type="text" name="cname" id="cname" />
		</td>
		</tr>
		
		<tr>		
		<td>
		<div class="field">
		  <p>
		   Name :
		  </p>
				
		</div>						
		<input class="inputbox" type="text" name="name" id="name" /></td>
		</tr>		
		
		<tr>		
		<td>
		<div class="field">
		  <p>
		   Phone number :
		  </p>
				
		</div>						
		<input class="inputbox" type="text" name="num" id="num" /></td>
		</tr>	

		<tr>		
		<td>
		<div class="field">
		  <p>
		   Email :
		  </p>
		
		</div>						
		<input class="inputbox" type="text" name="email" id="email" />
		</td>
		</tr>			
		
		<tr>		
		<td>
		<div class="field">
		  <p style="padding-bottom:5px;">
		  Service required
		  </p>		
		</div>						
		<input type="checkbox" name="chbox[]" value="Web Design"/><label id="checkn">Web Design</label>
		<input type="checkbox" name="chbox[]" value="SEO"/><label id="checkn">SEO</label>
		<input type="checkbox" name="chbox[]" value="SERM"/><label id="checkn">SERM</label>
		<input type="checkbox" name="chbox[]" value="SMM"/><label id="checkn">SMM</label>
		<input type="checkbox" name="chbox[]" value="PPC"/><label id="checkn">PPC</label>
		</td>
		</tr>		
		
		<tr>		
		<td>
		<div class="field">
		  <p>
		  Message :
		  </p>
				
		</div>						
		<textarea rows="5" cols="20" name="message" id="message" ></textarea></td>
		</tr>
		
		<tr>		
		<td>
		<div class="field">
		  <p>
		   Are You Human 5+3 = ? :
		  </p>
				
		</div>						
		<input class="inputbox" type="text" name="puzzle" id="puzzle" /></td>
		</tr>
	
		<tr>
		<td colspan=2>
			<input type="submit" onclick="showMessage()" class="button" value="Submit" />
		</td>
		</tr>
	</table>
		
	</form>
</div>
	
<div class="clear"></div>
		</div>
				
				<div class="first-block-right">
				
				<div id="formright">
<div id="cm_gray_top">
			
			<div id="cm_form_business_info">
			<p id="cm_form_business_name">E-Intelligence - India</p>
			<div>
			<div style="float: left;">
			9B, Ram Krishna Chambers,
			<br />
			B.P.C Road, Alkapuri <br />Vadodara - 390005
			<br />
			Gujarat, India
			</div>
			</div>
			<div style="clear: both;"></div>
			<div id="cm_form_email_phone_container"> <br />
			<b style="color: #666;">HR & General enquiries</b>			
			<p id="cm_form_phone">						
			<span id="cm_form_phone_text">+91-265-3954965</span>
			</p>			
			<p id="cm_form_email">			
			<a href="mailto:info@e-intelligence.in">info@e-intelligence.in</a>
			</p>
			<p id="cm_form_email">			
			<a href="mailto:hr@e-intelligence.in">careers@e-intelligence.in</a>
			</p>
			<br />
			<b style="color: #666;">Sales Enquiries</b> 
			<p id="cm_form_phone">						
			<span id="cm_form_phone_text">+91-265-3954968</span>
			</p>
			<!-- <p id="cm_form_email">			
			<a href="mailto:sales@e-intelligence.in">sales@e-intelligence.in</a>
			</p>
			-->
			<p id="social" style="padding-top: 10px;">
			<a href="https://www.facebook.com/eIntelligence" target="_blank"><img src="images/im_facebookh.png" width="16" height="16" /></a>
			<a href="http://twitter.com/eintelligence" target="_blank"><img src="images/im_twitterh.png" width="16" height="16" /></a>
			<a href="http://www.linkedin.com/company/e-intelligence" target="_blank"><img src="images/im_linkdinh.png" width="16" height="16" /></a>
			<a href="https://plus.google.com/u/0/b/117066953688422037339/" target="_blank"><img src="images/im_g-plush.png" width="16" height="16" /></a>
			</p>
			</div>			
			</div>
</div>
</div>		
 				</div>
			 </div>
             </div>
             </div>
<?php
/* * Home Page Block */
?>

<?php include("footer.php");?>		